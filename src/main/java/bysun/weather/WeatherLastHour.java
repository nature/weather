package bysun.weather;

import java.io.Serializable;

/**
 * 天气信息 最近一小时
 * eg
 * http://www.weather.com.cn/data/sk/101010100.html
 */
public class WeatherLastHour implements Serializable {
    private static final long serialVersionUID = 1335815981112791454L;
    private String city;
    private String cityid;
    private String temp;
    private String wd;
    private String ws;
    private String sd;
    private String wse;
    private String time;
    private String isradar;
    private String radar;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getIsradar() {
        return isradar;
    }

    public void setIsradar(String isradar) {
        this.isradar = isradar;
    }

    public String getRadar() {
        return radar;
    }

    public void setRadar(String radar) {
        this.radar = radar;
    }

    public String getSd() {
        return sd;
    }

    public void setSd(String sd) {
        this.sd = sd;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWd() {
        return wd;
    }

    public void setWd(String wd) {
        this.wd = wd;
    }

    public String getWs() {
        return ws;
    }

    public void setWs(String ws) {
        this.ws = ws;
    }

    public String getWse() {
        return wse;
    }

    public void setWse(String wse) {
        this.wse = wse;
    }
}
